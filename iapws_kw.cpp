#include "iapws_kw.h"

#include<cmath>

IAPWS_Kw::IAPWS_Kw(const double& extTK, const double& extrho)
  :
  TK(extTK),
  rho(extrho),
  molalterm(2.0*log10(18.015268e-3)),
  alfa0(-0.864671),
  alfa1(8659.19),
  alfa2(-22786.2),
  beta0(0.642044),
  beta1(-56.8534),
  beta2(-0.375754),
  gamma0(6.141500e-1),
  gamma1(4.825133e4),
  gamma2(-6.770793e4),
  gamma3(1.010210e7),
  arcis_alfa0(1.14387),
  arcis_alfa1(7923.28),
  arcis_alfa2(96276.7),
  arcis_beta0(2.00935),
  arcis_beta1(-3.87984),
  arcis_beta2(-1.542),
  rho0(1.0),
  invT(0.)
{
  invT = 1.0/TK;
}

IAPWS_Kw::~IAPWS_Kw()
{
}

double IAPWS_Kw::pK(){
  invT = 1.0/TK;
  return pK(invT, rho*1.0e-3);
}

double IAPWS_Kw::arcis_pK(){
  invT = 1.0/TK;
  return arcis_pK(invT, rho*1.0e-3);
}

double IAPWS_Kw::pKwG(const double& invT){
  return gamma0+invT*(gamma1+invT*(gamma2+gamma3*invT));
}

double IAPWS_Kw::Q(const double& invT, const double& rho){
  double myQ(alfa0+invT*(alfa1+alfa2*invT*pow(rho,2./3.)));
  myQ = exp(myQ);
  return myQ*rho/rho0;
}

double IAPWS_Kw::arcis_Q(const double& invT, const double& rho){
  double myQ(arcis_alfa0+invT*(arcis_alfa1+arcis_alfa2*invT*pow(rho,2./3.)));
  myQ = exp(myQ);
  return myQ*rho/rho0;
}

double IAPWS_Kw::pK(const double& invT, const double& rho){
  double q(Q(invT, rho));
  double pK(rho*(beta0+beta1*invT+beta2*rho));
  pK *= q/(q+1.0);
  pK *= -1.0;
  pK += log10(q+1.0);
  pK *= -12.0;
  pK += pKwG(invT);
  pK += molalterm;
  return pK;
}

double IAPWS_Kw::arcis_pK(const double& invT, const double& rho){
  double q(arcis_Q(invT, rho));
  double pK(rho*(arcis_beta0+arcis_beta1*invT+arcis_beta2*rho));
  pK *= q/(q+1.0);
  pK *= -1.0;
  pK += log10(q+1.0);
  pK *= -12.0;
  pK += pKwG(invT);
  pK += molalterm;
  return pK;
}
