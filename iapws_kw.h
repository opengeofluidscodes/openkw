class IAPWS_Kw{
 public:
  IAPWS_Kw(const double& extTK, const double& extrho);
  ~IAPWS_Kw();
  double pK();
  double arcis_pK();

 private:
  const double& TK;
  const double& rho; 
  const double molalterm;
  const double alfa0;
  const double alfa1;
  const double alfa2;
  const double beta0;
  const double beta1;
  const double beta2;
  const double gamma0;
  const double gamma1;
  const double gamma2;
  const double gamma3;
  const double arcis_alfa0;
  const double arcis_alfa1;
  const double arcis_alfa2;
  const double arcis_beta0;
  const double arcis_beta1;
  const double arcis_beta2;
  const double rho0;
  
  double        invT;
  double pKwG(const double& invT);
  double Q(const double& invT, const double& rho);
  double arcis_Q(const double& invT, const double& rho);
  double pK(const double& invT, const double& rho);
  double arcis_pK(const double& invT, const double& rho);
};
