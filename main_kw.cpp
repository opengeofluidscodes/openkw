#include<iostream>
#include<fstream>
#include<cmath>
#include "iapws_kw.h"
#include "steam4.h"

using namespace std;

int main()
{
  double     rho(0.);
  double     TK(0.);
  double     TC(0.);
  double     p(0.);
  IAPWS_Kw   kw(TK,rho);

  Prop *prost;
  Prop *satv;
  Prop *satl;
  prost = newProp('T', 'p', 1);
  satv  = newProp('T', 'p', 1);
  satl  = newProp('T', 'p', 1);
  // int i{1};
  // while(i > 0)
  // {
  //     cout << "Please enter T[K]  : "; cin >> TK;
  //     cout << "Please enter rho   : "; cin >> rho;
  //     cout << "1/T = " << 1./TK << endl;
  //     cout << "Kw  = " << kw.pK() << "\t" << kw.arcis_pK() << endl << endl;
  // }



  // isochores
  ofstream ofile("isochores_arcis_validrange.dat");
  for( rho = 200.; rho < 1000.1; rho += 100.)
    {
      ofile << "# " << rho << " kg/m3\n";
      cout << "doing rho = " << rho << endl;
      for(TK = 298.15; TK < 674./*1074.*/; TK += 1.)
        {
          cout << ".";
          if(TK < 373.976+273.15){
            sat_t( TK, satl, satv );
            if(rho < 322. && rho > satv->d ) continue;
            else if( rho > 322. && rho < satl->d ) continue;
            else
              {
                water_td(TK,rho,prost);
                if(prost->p > 310.e5) continue;
                ofile << 1./TK << "\t" << kw.arcis_pK() << "\t" << TK-273.15 << endl;
              }
          }
          else
            {
              water_td(TK,rho,prost);
              if(prost->p > 310.e5) continue;
              ofile << 1./TK << "\t" << kw.arcis_pK() << "\t" << TK-273.15 << endl;
            }
        }
      ofile << endl;
      cout << endl;
    }
  cout << "doing saturated vapor ";
  for(TK = 298.15; TK < 373.976+273.15; TK += 1.)
    {
      cout << ".";
      sat_t( TK, satl, satv );
      rho = satv->d;
      if(rho < 100.) continue;
      ofile << 1./TK << "\t" << kw.arcis_pK() << "\t" << TK-273.15 << endl;
    }
  cout << "doing saturated liquid ";
  for(TK = 373.976+273.15-0.5; TK > 298.; TK -= 1.)
    {
      cout << ".";
      sat_t( TK, satl, satv );
      rho = satl->d;
      ofile << 1./TK << "\t" << kw.arcis_pK() << "\t" << TK-273.15 << endl;
    }
  cout << "...";
  ofile << endl;
  cout << endl;

  // // //isotherms
  // // ofstream ofile("test.dat");
  // // TK = 300.;
  // // for(rho = 1.0; rho < 1.101; rho += 0.01){
  // //   ofile << log10(rho) << "\t" << kw.pK() << endl;
  // // }
  // // ofile << endl;

  // // TK = 400.;
  // // for(rho = 1.0; rho < 1.101; rho += 0.01){
  // //   ofile << log10(rho) << "\t" << kw.pK() << endl;
  // // }
  // // ofile << endl;
  
  // // TK = 500.;
  // // for(rho = 0.9; rho < 1.101; rho += 0.01){
  // //   ofile << log10(rho) << "\t" << kw.pK() << endl;
  // // }
  // // ofile << endl;
  
  // // TK = 600.;
  // // for(rho = 0.8; rho < 1.101; rho += 0.01){
  // //   ofile << log10(rho) << "\t" << kw.pK() << endl;
  // // }
  // // ofile << endl;
  
  // // for(TK=650.; TK <= 1073.15; TK += 50.){
  // //   for(rho = 0.1; rho < 1.101; rho += 0.05){
  // //     ofile << log10(rho) << "\t" << kw.pK() << endl;
  // //   }
  // //   ofile << endl;
  // // }
  freeProp(prost);
  freeProp(satv);
  freeProp(satl);

  
}

